<?php
/**
* PHP class for one-way hashing & validation plus two-way encryption
* @copyright 2015 Philip K Meadows, All Rights Reserved
* @author Philip K Meadows
* 
* This is a derived works based on code from various authors with my own amendments
* My amendmenments should be enough that the application of "sweat of the brow" applies
* 
* ##### ACKNOWLEDGEMENTS #####
* 
* The methods passwordCreate(), passwordValidate() & slowEquals() are derived works of Taylor Hornby
* @source (https://crackstation.net/hashing-security.htm#phpsourcecode)
* Original methods from source; create_hash(), validate_password() & slow_equals() are @copyright 2013 Taylor Hornby
* 
* The methods encrypt() and decrypt() are derived works of Josh Hartman
* @source (https://gist.github.com/joshhartman/10342187)
* However, it has flaws as pointed out by Taylor Hornby, so have modified accordingly
* @source (http://www.cryptofails.com/post/121201011592/reasoning-by-lego-the-wrong-way-to-think-about)
* 
* The method pbkdf2() is @copyright 2013 Taylor Hornby - see more notes further down, above the method declaration
* @source (https://defuse.ca)
* 
* Shoutout to http://thefsb.tumblr.com, whoever you are, for notes on openSSL stuff
* @source (http://thefsb.tumblr.com/post/110749271235/using-opensslendecrypt-in-php-instead-of)
* 
* ##### REDISTRIBUTION #####
*
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice and notes, this list of conditions
* AND the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice and notes, this list of conditions
* AND the following disclaimer in the documentation and/or other materials provided with the distribution.
* 
* ##### DISCLAIMER #####
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
* feel free to comment out the namespace or rename it to suit your needs
*/
namespace WebGuyPhil\PHPCryptography;

class PHPCryptography
{
	
	protected $theClass;
	protected $canCompress;
	protected $algorithm;
	protected $cipher;
	protected $size;
	protected $pbkdf2Iterations;
	
	public function __construct()
	{
		$this->theClass = get_class();
		// check we got what we need
		if( ! file_exists( '/dev/urandom' ) 
		&& ! function_exists( 'openssl_random_pseudo_bytes' ) 
		&& ! function_exists( 'mcrypt_create_iv' ) )
		{
			trigger_error( $this->theClass . ' ERROR: No ability to create salt!', E_USER_ERROR );
		}
		$compression = FALSE;
		if( function_exists( 'gzdeflate' ) && function_exists( 'gzinflate') )
		{
			$compression = TRUE;
		}
		$this->canCompress = $compression;
		// Use hash_algos() to get array of available on your server and stick some >= 256bit algos in this array
		$algos = array(
			'sha256',
			'sha512',
			'ripemd256',
			'ripemd320',
			'whirlpool',
			);
		$algoKey = array_rand( $algos );
		$this->algorithm = $algos[$algoKey];
		$this->cipher = 'AES-256-CBC'; // Use openssl_get_cipher_methods() to get array of available
		$this->size = 32; // Used for salt and PBKDF2 initial lengths
		$minPbkdf2Iterations = 20000; // Feel free to adjust the min and max values depending on CPU Speed
		$maxPbkdf2Iterations = 60000;
		$this->pbkdf2Iterations = rand( $minPbkdf2Iterations, $maxPbkdf2Iterations );
	} // END __construct()
	
	/**
	* Generates a random AF salt
	* We have this function separate so you can store different salts for differents users if you like
	* 
	* @return string: the generated salt
	*/
	public function createSalt()
	{
		if( file_exists( '/dev/urandom' ) )
		{
			$salt = NULL;
			$f = fopen( '/dev/urandom', 'rb' );
			stream_set_read_buffer( $f, $this->size );
			$salt = fread( $f, $this->size );
			fclose( $f );
			return bin2hex( $salt );
		}
		elseif( function_exists( 'openssl_random_pseudo_bytes' ) )
		{
			$strong = TRUE;
			return bin2hex( openssl_random_pseudo_bytes( $this->size, $strong ) );
		}
		elseif( function_exists( 'mcrypt_create_iv' ) )
		{
			return bin2hex( mcrypt_create_iv( $this->size, MCRYPT_DEV_URANDOM ) );
		}
		else
		{
			trigger_error( $this->theClass . ' ERROR: Salt generation fail!', E_USER_ERROR );
		}
	}
	
	/**
	* Creates a random Initialization Vector (IV) for one-time use in encryption/decryption
	* Uses the createSalt() method
	* 
	* @return string: a correct length IV for the current cipher
	*/
	// NOTE: protected
	protected function createIV()
	{
		if ( function_exists( 'mb_substr' ) )
		{
		    $iv = mb_substr( $this->createSalt(), 0, 16, '8bit' );
		}
		else
		{
		    $iv = substr( $this->createSalt(), 0, 16 );
		}
		return $iv;
	}
	
	/**
	* Prepares payload for Encrypting
	* @param mixed string/array/object $payload
	* 
	* @return mixed string/binary
	*/
	protected function box( $payload )
	{
		if( $this->canCompress === TRUE )
		{
			return gzdeflate( json_encode( $payload ) );
		}
		else
		{
			return json_encode( $payload );
		}
	}
	
	/**
	* Unwraps gift returned from Decryption
	* @param mixed string/binary $payload
	* 
	* @return mixed string/array/object
	*/
	protected function unbox( $payload )
	{
		if( $this->canCompress === TRUE )
		{
			return json_decode( gzinflate( $payload ), TRUE );
		}
		else
		{
			return json_decode( $payload, TRUE );
		}
	}
	
	/**
	* Encrypts stuff
	* Uses "Encrypt then MAC (Message Authentication Code)" (EtM) design
	* 
	* @param string OR object/array $hashMe: the string to be encrypted
	* @param string $salt: the salt to use in encryption
	* 
	* @return string $output: the encrypted version of $hashMe
	*/
	public function encrypt( $hashMe, $salt )
	{
		$hashMe = $this->box( $hashMe );
		$iv = $this->createIV();
		$encrypted = bin2hex( openssl_encrypt( $hashMe, $this->cipher, $salt, OPENSSL_RAW_DATA, $iv ) );
        $mac = hash_hmac( $this->algorithm, $iv.$encrypted, $salt );
	    $output = array();
	    $output[] = $encrypted;
	    $output[] = $salt;
	    $output[] = $iv;
	    $output[] = $mac;
	    $output[] = bin2hex( base64_encode( $this->cipher ) );
	    $output[] = bin2hex( base64_encode( $this->algorithm ) );
	    $output = implode( '|', $output );
		return $output;
	}
	
	/**
	* Decrypts stuff
	* All comparisons use timing safe method slowEquals() @copyright 2013 Taylor Hornby
	* 
	* @param string $unHashMe: the string to be decrypted
	* @param string $salt: the salt to use in decryption
	* 
	* @return string OR array $output: the decrypted version of $unHashMe
	*/
	public function decrypt( $unHashMe, $salt )
	{
		$parts = explode( '|', $unHashMe );
		$hashSalt = $parts[1];
		if( $this->slowEquals( $salt, $hashSalt ) === FALSE )
		{
			trigger_error( $this->theClass . ' DECRYPT SALT ERROR: Possible hack detected!', E_USER_ERROR );
			// or you could return FALSE if you want to do something else
		}
		$hashed = $parts[0];
		$iv = $parts[2];
        $mac = $parts[3];
		$cipher = base64_decode( hex2bin( $parts[4] ) );
		$algo = base64_decode( hex2bin( $parts[5] ) );
        $calcmac = hash_hmac( $algo, $iv.$hashed, $salt );
        if( $this->slowEquals( $mac, $calcmac ) === FALSE )
        {
            trigger_error( $this->theClass . ' DECRYPT MAC ERROR: Possible hack detected!', E_USER_ERROR );
			// or you could return FALSE if you want to do something else
        }
		$decrypted = openssl_decrypt( hex2bin( $hashed ), $cipher, $salt, OPENSSL_RAW_DATA, $iv );
        $decrypted = $this->unbox( $decrypted );
        return $decrypted;
	}
	
	/**
	* One-way hashes a string never to be unhashed
	* 
	* @param string $hashMe: raw string/password to be hashed
	* 
	* @return string $output: the hashed string/password
	*/
	public function passwordCreate( $hashMe )
	{
	    $salt = $this->createSalt();
	    $output = array();
	    $output[] = $this->pbkdf2( $this->algorithm, $hashMe, $salt, $this->pbkdf2Iterations );
	    $output[] = $salt;
	    $output[] = bin2hex( base64_encode( $this->pbkdf2Iterations ) );
	    $output[] = bin2hex( base64_encode( $this->algorithm ) );
	    $output = implode( '|', $output );
	    return $output;
	} // END passwordCreate
	
	/**
	* Checks a string against a previously stored one-way hash
	* All comparisons use timing safe method slowEquals() @copyright 2013 Taylor Hornby
	* 
	* @param string $userInput: raw password entered by user
	* @param string $correctHash: the correct hash currently in the database
	* 
	* @return bool TRUE on success, FALSE on fail
	*/
	public function passwordValidate( $userInput, $correctHash )
	{
	    $params = explode( '|', $correctHash );
	    $theHash = $params[0];
	    $algorithm = base64_decode( hex2bin( $params[3] ) );
	    $salt = $params[1];
	    $iterations = base64_decode( hex2bin( $params[2] ) );
	    $attemptedHash = $this->pbkdf2( $algorithm, $userInput, $salt, $iterations );
	    if( $this->slowEquals( $theHash, $attemptedHash ) === TRUE )
	    {
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	} // END passwordValidate
	
	/**
	* Timing safe equals comparison of two strings $a and $b
	* @copyright 2013 Taylor Hornby
	* 
	* @param string $a
	* @param string $b
	* 
	* @return bool TRUE if match, FALSE if fail
	*/
	// NOTE: protected
	protected function slowEquals( $a, $b )
	{
	    $diff = strlen( $a ) ^ strlen( $b );
	    for( $i = 0; $i < strlen( $a ) && $i < strlen( $b ); $i++ )
	    {
	        $diff |= ord( $a[$i] ) ^ ord( $b[$i] );
	    }
	    if( $diff === 0 )
	    {
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	} // END slowEquals
	
	/**
	 * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
	 * 
	 * @param string $algorithm: The hash algorithm to use
	 * @param string $hashMe: The string to hash
	 * @param string $salt: A salt that is unique to the string to hash
	 * @param int $iterationCount: Iteration count. Higher is better, but slower
	 * @param int $keyLength: The length of the derived key in bytes
	 * @param bool $rawOutput: If TRUE, the key is returned in raw binary format. Hex encoded otherwise
	 * @return string: A $keyLength-byte key derived from the string to hash and salt
	 *
	 * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
	 *
	 * This implementation of PBKDF2 was originally created by https://defuse.ca
	 * With improvements by http://www.variations-of-shadow.com
	 */
	// NOTE: protected
	protected function pbkdf2( $algorithm, $hashMe, $salt, $iterationCount, $rawOutput = FALSE )
	{
		$keyLength = $this->size;
	    if( ! in_array( $algorithm, hash_algos(), TRUE ) )
	    {
			trigger_error( $this->theClass . ' PBKDF2 ERROR: Invalid hash algorithm.', E_USER_ERROR );
		}	        
	    if( $iterationCount <= 0 || $keyLength <= 0 )
	    {
			trigger_error( $this->theClass . ' PBKDF2 ERROR: Invalid parameters.', E_USER_ERROR );
		}	        

	    if( function_exists( 'hash_pbkdf2' ) )
	    {
	        // The output length is in NIBBLES (4-bits) if $rawOutput is false!
	        if( ! $rawOutput )
	        {
	            $keyLength = $keyLength * 2;
	        }
	        return hash_pbkdf2( $algorithm, $hashMe, $salt, $iterationCount, $keyLength, $rawOutput );
	    }

	    $hashMeLength = strlen( hash( $algorithm, '', TRUE ) );
	    $blockCount = ceil( $keyLength / $hashMeLength );
	    $output = '';
	    for( $i = 1; $i <= $blockCount; $i++ )
	    {
	        // $i encoded as 4 bytes, big endian.
	        $last = $salt . pack( "N", $i );
	        // first iteration
	        $last = $xorsum = hash_hmac( $algorithm, $last, $hashMe, TRUE );
	        // perform the other $iterationCount - 1 iterations
	        for ( $j = 1; $j < $iterationCount; $j++ )
	        {
	            $xorsum ^= ( $last = hash_hmac( $algorithm, $last, $hashMe, TRUE ) );
	        }
	        $output .= $xorsum;
	    }
	    if( $rawOutput )
	    {
			return substr( $output, 0, $keyLength );
		}
		else
		{
			return bin2hex( substr( $output, 0, $keyLength ) );
		}	        
	} // END pbkdf2
	
} // END class PHPCryptography
